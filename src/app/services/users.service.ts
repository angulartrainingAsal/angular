import { Injectable, ViewChild} from '@angular/core';
// httpClient 
import { HttpClient, HttpHeaders, HttpResponse, } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from "rxjs/Rx";
// toaster modules
import { ToastrService, ToastContainerDirective } from 'ngx-toastr';
// HTTP Options
const httpOptions = {
    headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
    })
};
@Injectable()
export class UsersService {
    @ViewChild(ToastContainerDirective) toastContainer: ToastContainerDirective;
    // server url
    readonly rootURL: string = "http://172.22.1.111:8080/auth/signup";
    constructor(private http: HttpClient, private toastr: ToastrService) { }

    public postUser(abj: any) {
        this.http.post(this.rootURL,abj )
        .catch((err) => {
            if(err.error.message !=undefined){
            this.toastr.error(err.error.message);
        }
        else{
            this.toastr.error(err.message);
        }
            return Observable.throw(err)
        })
        .subscribe(
            (data: any) => { 
            try{
                this.toastr.success(data.message);
                console.log(data);
            }
            catch(e){
                this.toastr.error("internal server Error" + e);
            }      
           
            });
    }
 
     

}//end class
