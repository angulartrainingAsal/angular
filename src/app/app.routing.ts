//Modules
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders, NgModule } from '@angular/core';
//Componenets
import { NavbarComponent } from './componenets/navbar/navbar.component';
import { MainComponent } from './componenets/main/main.component';
import { SignupComponent } from './componenets/signup/signup.component';
import { ChatComponent } from './componenets/chat/chat.component';
import { ChatRoomsComponent } from './componenets/chatRooms/chatRooms.component';

const routes: Routes = [
  {path: 'signup', component: SignupComponent, children:[
    {
      path: '', component: NavbarComponent, outlet: 'navbar'
    }
  ]  },
  {path: 'main', component: MainComponent, children:[
    {
      path: '', component: NavbarComponent, outlet: 'navbar'
    }
  ]  },
  {path: 'chat', component: ChatComponent, children:[
    {path: '', component:NavbarComponent, outlet:'navbar'}
  ]},
  {path: 'signupTwo', component: SignupComponent},
  {path: 'chatRoom', component: ChatRoomsComponent, children:[
    {path: '', component:NavbarComponent, outlet:'navbar'}
  ]},


  {path: '', redirectTo: 'main', pathMatch: 'full'},
  {path: '**', redirectTo: 'main', pathMatch:'full'},
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes);
