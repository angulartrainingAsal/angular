import { Component, OnInit } from '@angular/core';
// Form modules
import { FormGroup, FormsModule, ReactiveFormsModule, FormControl, Validators, FormBuilder, FormArray, AbstractControl, ValidationErrors } from '@angular/forms';
// toaster modules
import { ToastrService, ToastContainerDirective } from 'ngx-toastr';
//router
import { Router } from '@angular/router';
//Rainbow Sdk
import { sdk } from './../../../modules/sdk';

@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
    title = 'Alcatel-Lucent Enterprise - Rainbow CPAAS';
    version = sdk.version();
    form: FormGroup;// signup form

    constructor(public fb: FormBuilder, private toastr: ToastrService, private router: Router) {

        //sdk.createBubble("bashar","bashar");
    }
    // create login form
    createForm() {
        var emailVal = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
        this.form = this.fb.group({
            // controles 
            "email": ['', [Validators.required, Validators.pattern(emailVal)]],
            "password": ['', Validators.required],
        });


    }
    // login button event
    async onSubmit() {
        try {
            await sdk.login(this.form.controls['email'].value, this.form.controls['password'].value).then((account) => {
                if (sdk.loggedin === true) {
                    this.toastr.success("Welcome " + this.form.controls['email'].value);
                    this.router.navigateByUrl('chatRoom');
                }
                else {
                    this.toastr.error("Email or Password is incorrect, Please try again later.");
                }
            })
                .catch((err) => {
                    this.toastr.error("Email or Password is incorrect, Please try again later.");
                });
        }
        catch (e) {
            //this.submited = false;
            this.toastr.error("Email or Password is incorrect, Please try again later.");
            console.log(e);

        }
    }
    ngOnInit() {
        this.createForm();
    }


}
