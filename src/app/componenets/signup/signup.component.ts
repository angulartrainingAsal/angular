import { Component, OnInit, Directive, HostListener, ElementRef, Input, ViewChild } from '@angular/core';
// toaster modules
import { ToastrService, ToastContainerDirective } from 'ngx-toastr';
// Form modules
import { FormGroup, FormsModule, ReactiveFormsModule, FormControl, Validators, FormBuilder, FormArray, AbstractControl, ValidationErrors } from '@angular/forms';
// httpClient
import { UsersService } from './../../services/users.service';

// metadata

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],

})
export class SignupComponent implements OnInit {
  @ViewChild(ToastContainerDirective) toastContainer: ToastContainerDirective;
  form: FormGroup;// signup form
  submited: boolean;// used to show submited alert
  response: any;
  // server url
  readonly rootURL: string = "http://172.22.1.111:8080/auth/signup";
  // Data
  companies = ["ASAL", "Masar", "Sah", "Audi"];
  positions = ["Developer", "Tester", "Back-End Developer"];
  nieghbourhoods = ["Doleam", "Makmata"];
  buildings = ["13-22", "22-46", "31-45"];
  floors = [1, 2, 3, 4, 5, 6, 7, 8];
  apartments = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  residencyType = ["Permanent", "Temporary"];
  constructor(public fb: FormBuilder, private userService: UsersService, private el: ElementRef, private toastr: ToastrService) {
    this.submited = null;
  }
  //phone text field.
  @Input() NumericInput: boolean;
  @HostListener('keydown', ['$event']) onKeyDown(event) {
    let evt = <KeyboardEvent>event;
    if (this.NumericInput == true) {
      // Unicode for allowed key down.
      //Only allow numbers to input into field of Numeric Input = true
      if ((evt.which >= 48 && evt.which <= 57) || (evt.which >= 96 && evt.which <= 105) || evt.which == 8 || evt.which == 46 || evt.which == 37 || evt.which == 39) { }
      else {
        evt.preventDefault();
      }
    }

  }
  // Form builder
  createForm() {
    var emailVal = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
    var phoneVal = /^05[0-9]{8}$/;
    this.form = this.fb.group({
      // controles 
      "firstName": ['', Validators.required],
      "lastName": ['', Validators.required],
      "email": ['', [Validators.required, Validators.pattern(emailVal)]],
      "employee": false,
      "company": "",
      "company_id": "5adc3b81ee48b30444eb6348",
      "department": "",
      "department_id": "5adc3b81ee48b30444eb6348",
      "resident": false,
      "residency_type": "",
      "neighbourhood_name": "",
      //"nieghbourhood_id": "5adc3b81ee48b30444eb6348",
      "building_number": "",
      "building_id": "5adc3b81ee48b30444eb6348",
      "floor_number": "",
      "floor_id": "5adc3b81ee48b30444eb6348",
      "apartment_number": "",
      "apartment_id": "5adc3b81ee48b30444eb6348",
      "mobile": ["", Validators.pattern(phoneVal)]
    });

  }
  //submit button
  onSubmit() {
    try {
      this.postUser();
      this.submited = true;
      //this.form.reset();
      this.ngOnInit();
    }
    catch (e) {
      this.submited = false;
      console.log(e);

    }
  }
  //post user
  public postUser() {
    console.log(this.form.value);
    this.userService.postUser(this.form.value);
  }
  // Yes No Q
  toggle(isOn: boolean, control: string) {
    this.form.controls[control].setValue(isOn);
  }
  ngOnInit() {
    //this.createForm();
    this.createForm();
    this.form.controls['employee'].setValue(false);
    this.form.controls['resident'].setValue(false);
    this.toastr.overlayContainer = this.toastContainer;
  }

}
