import { Component, OnInit } from '@angular/core';
// translateService
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  show:boolean = false;
  constructor(private translate: TranslateService) { }


  switchLanguage( language: string){
    this.translate.use(language);
    console.log('Language is '+ language);
  }
  toggleCollapse() {
    this.show = !this.show
  }
  ngOnInit() {
  }

}
