import { async } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
// Form modules
import { FormGroup, FormsModule, ReactiveFormsModule, FormControl, Validators, FormBuilder, FormArray, AbstractControl, ValidationErrors } from '@angular/forms';
import { ToastrService, ToastContainerDirective } from 'ngx-toastr';
//router
import { Router } from '@angular/router';
//ranbow sdk APIs
import { sdk, SDK } from './../../../modules/sdk';
import { Jsonp } from '@angular/http';



@Component({
  selector: 'app-chatRooms',
  templateUrl: './chatRooms.component.html',
  styleUrls: ['./chatRooms.component.scss']
})
export class ChatRoomsComponent implements OnInit {
  form: FormGroup;
  // last message received
  lastMessage: String;
  // my network contacts names
  contacts: string[];
  // used to save contact name when click on it
  contact: string;
  conversationHistory: String[];
  constructor(public fb: FormBuilder, private toastr: ToastrService, private router: Router, private toast: ToastrService) {
    //authentication
    //this.lastMessage=sdk.lastmessage;
   

    if (sdk.loggedin != true) {
      this.toastr.error("Rainbow has stopped connection !");
      this.router.navigateByUrl('chat');
     
    }

    this.getAllContacts();
  }

  createForm() {
    this.form = this.fb.group({
      // controles 
      "message": ['', Validators.required],
    });
  }

  sendMessageToContact(contactId) {
    if (this.contact != null) {
      this.lastMessage = sdk.lastmessage;
      console.log("chat room: " + this.lastMessage);
      sdk.sendMessageToContact(contactId, this.form.controls['message'].value);
      this.form.controls['message'].setValue('');
    }
    else {
      this.toast.error("Please chose a contact.")
    }

  }

  public async getAllContacts() {
    try {
      sdk.getAllContacts();
      this.contacts = await sdk.contactsName;
      this.contacts.length = await sdk.contactsName.length;
      //console.log(this.contacts.length);
    }
    catch (err) {
      console.log("There is no user logged in, Please login then get this page. Thanks,");
    }
  }

  public async  onContactClick(name) {
    // show selected name on left hand div.
    this.contact = name;
    //search for a contct, open conversation and retrieve the data.
    await sdk.searchContactByName(name);

    this.conversationHistory = await sdk.conversationHistory;
    console.log(this.conversationHistory.length);



  }
  public logout() {
    try {
      sdk.signout();
      this.toast.show("Logged out");
      this.router.navigateByUrl('chat');
    }
    catch (err) {
      this.toast.error("Something error while signout.")
    }
  }

  ngOnInit() {
    this.createForm();
    this.lastMessage = sdk.lastmessage;

  }

}
