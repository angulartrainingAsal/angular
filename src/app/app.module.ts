// Angular modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule, ToastContainerModule } from 'ngx-toastr';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';

// Forms Builder
import {
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  FormControl,
  Validators,
  FormBuilder,
  FormArray,
  AbstractControl
} from '@angular/forms';
//routes
import { AppRoutes } from './app.routing';
//Services
import { UsersService } from './services/users.service';
// components
import { SignupComponent } from './componenets/signup/signup.component';
import { AppComponent } from './app.component';
import { MainComponent } from './componenets/main/main.component';
import { NavbarComponent } from './componenets/navbar/navbar.component';
import { ChatComponent } from './componenets/chat/chat.component';
import { ChatRoomsComponent } from './componenets/chatRooms/chatRooms.component';
//classes

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    NavbarComponent,
    MainComponent,
    ChatComponent,
    ChatRoomsComponent



  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    AppRoutes,
    CommonModule,
    BrowserAnimationsModule,
    ToastContainerModule,
    HttpModule,
    ToastrModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),

  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
