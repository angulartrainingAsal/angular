// async functions
import { async } from '@angular/core/testing';
//router
import { Router } from '@angular/router';
//sdk variables
declare var rainbowSDK: any;
declare var angular: any;
declare var $: any;
declare var room: any;
export class SDK {
    lastmessage: string;
    loggedin: boolean;
    contactsName: string[];
    searchContact: string;
    conversationHistory: String[];
    userName: String;
    password: String;


    initialize() {
        console.log("[DEMO] :: Starter-Kit of the Rainbow SDK for Web with React started!");

        var applicationID = "",
            applicationSecret = "";

        /* Bootstrap the SDK */
        angular.bootstrap(document, ["sdk"]).get("rainbowSDK");

        /* Callback for handling the event 'RAINBOW_ONREADY' */
        var onReady = () => {
            console.log("[DEMO] :: On SDK Ready !");

            /*-------------*/
        };

        /* Callback for handling the event 'RAINBOW_ONCONNECTIONSTATECHANGED' */
        var onLoaded = function onLoaded() {
            console.log("[DEMO] :: On SDK Loaded !");

            rainbowSDK.initialize().then(function () {
                console.log("[DEMO] :: Rainbow SDK is initialized!");

            }).catch(function () {
                console.log("[DEMO] :: Something went wrong with the SDK...");
            });


        };

        var onConnectionStateChangeEvent = function onConnectionStateChangeEvent(event, status) {

            switch (status) {
                case rainbowSDK.connection.RAINBOW_CONNECTIONCONNECTED:
                    console.log('RAINBOW_CONNECTIONCONNECTED')

                    break;
                case rainbowSDK.connection.RAINBOW_CONNECTIONINPROGRESS:
                    console.log('RAINBOW_CONNECTIONINPROGRESS')

                    break;
                case rainbowSDK.connection.RAINBOW_CONNECTIONDISCONNECTED:
                    console.log('RAINBOW_CONNECTIONDISCONNECTED')

                    break;
                default:
                    break;
            };
        };
        var onNewMessageReceived = (event, message, conversation) => {
            rainbowSDK.im.markMessageFromConversationAsRead(conversation, message);
            // this.lastmessage = conversation.lastMessageText;
            console.log("sdk new message: " + this.lastmessage);
        };


        /* Listen to the SDK event RAINBOW_ONREADY */
        $(document).on(rainbowSDK.RAINBOW_ONREADY, onReady);
        /* Listen to the SDK event RAINBOW_ONLOADED */
        $(document).on(rainbowSDK.RAINBOW_ONLOADED, onLoaded);
        /* Subscribe to Rainbow connection change*/
        $(document).on(rainbowSDK.connection.RAINBOW_ONCONNECTIONSTATECHANGED, onConnectionStateChangeEvent);
        // Listen to new message received
        $(document).on(rainbowSDK.im.RAINBOW_ONNEWIMMESSAGERECEIVED, onNewMessageReceived);


        rainbowSDK.load();
    };

    get version() {

        return rainbowSDK.version;
    };

    public async login(userName, Password) {
        this.userName = userName;
        this.password = Password;
        // The SDK for Web is ready to be used, so you can sign in

        await rainbowSDK.connection.signin(userName, Password)
            .then((account) => {
                // You have successfully signed to Rainbow
                this.loggedin = true;
                console.log("The user " + userName + " loggedin successfully");
                this.getAllContacts();
            })
            .catch((err) => {

                // An error occurs (e.g. bad credentials)
                console.log("The user " + userName + "  failed to login \n" + JSON.stringify(err));
                this.loggedin = false;

            });


    };
    // Create a bubble on chat room
    public createBubble(name) {
        var ownedBubble;

        /* Handler called when the user clicks on a contact */
        rainbowSDK.bubbles.createBubble(name, "The description of the bubble").then(function (bubble) {
            ownedBubble = bubble;
            console.log("[DEMO] :: bubble created");

        }).catch(function (err) {
            console.log("[DEMO] :: Bubble name is already exists !");

        });
    };
    // sending a message to a contact by passing his id
    public async sendMessageToContact(contactId, message) {
        var selectedContactById = null;
        // var lastmessage2;
        /* Handler called when the user clicks on a contact */
        selectedContactById = rainbowSDK.contacts.getContactById(contactId);
        var associatedConversation = null;
        //open a conversation for contact of id = contactId
        await rainbowSDK.conversations.openConversationForContact(this.searchContact).then((conversation) => {
            associatedConversation = conversation;
            //this.lastmessage = selectedContactById.firstname + ": " + conversation.lastMessageText;
            // send a message to the contact
            rainbowSDK.im.sendMessageToConversation(associatedConversation, message);
            //  console.log("conv" + associatedConversation);
        }).catch((err) => {
            //  console.log("open conversation error");

        });

    }//end select contact

    //get all contacts
    public async getAllContacts() {
        var contacts: any[];
     
        contacts = await rainbowSDK.contacts.getAll();
        this.contactsName = await contacts;
        // store contacts into array to use them for openning conversation and send messages.
        for (var x in this.contactsName) {
            this.contactsName[x] = await contacts[x].displayName;
        }
   
    }
    //search a contact by name
    public async searchContactByName(name) {
        var selectedContact = null;
        await rainbowSDK.contacts.searchByName(name).then((contact) => {
            selectedContact = contact;
            // Ok, we have the contact object
            console.log("Ok, we have the contact object : " + selectedContact[0].firstname);
        }).catch((err) => {
            //Something when wrong with the server. Handle the trouble here
            console.log("Something when wrong with the server. Handle the trouble here");
        });
        this.searchContact = selectedContact[0];

        // get conversation attributes.
        var associatedConversation = null;
        var messages = null;
        await rainbowSDK.conversations.openConversationForContact(selectedContact[0]).then((conversation) => {
            associatedConversation = conversation;

            //this.lastmessage = selectedContact[0].firstname+": "+conversation.lastMessageText
            //  console.log(conversation.lastMessageText);
        }).catch((err) => {
            //Something when wrong with the server. Handle the trouble here

        });

        await rainbowSDK.im.getMessagesFromConversation(associatedConversation, 30);
        //store the conversation into array.
        this.conversationHistory = await associatedConversation.messages;
        for (var x in associatedConversation.messages) {
            this.conversationHistory[x] = await associatedConversation.messages[x].from.firstname + ": " + await associatedConversation.messages[x].data
        }





    }
    // logout from current session
    public signout() {
        rainbowSDK.connection.signout();
        this.loggedin = false;
    }
};



let sdk = new SDK();

export { sdk };